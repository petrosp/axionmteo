Rails.application.routes.draw do
  namespace :api do
   namespace :v1 do
     post 'login', to: 'sessions#create'
     delete 'logout', to: 'sessions#destroy'
     post 'signup', to: 'registrations#create'
     jsonapi_resources :accounts, only: [:show, :edit, :update]
     jsonapi_resources :users
    jsonapi_resources :countries
    get 'print/:id', to: 'pdf#print'
    get 'download', to: 'excel#download'
    put 'upload', to: 'excel#upload'
   end
 end
 get '/', to: 'application#not_found'
 get '*path', to: 'application#not_found'
 post '*path', to: 'application#not_found'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
