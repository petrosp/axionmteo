 namespace :app do
   task :setup => :environment do
     Rake::Task['db:create'].invoke
     Rake::Task['db:migrate'].invoke
     Rake::Task['db:seed:users'].invoke
   end
end    
