# frozen_string_literal: true

# ## Schema Information
#
# Table name: `users`
#
# ### Columns
#
# Name                   | Type               | Attributes
# ---------------------- | ------------------ | ---------------------------
# **`id`**               | `uuid`             | `not null, primary key`
# **`username`**         | `string`           | `not null`
# **`email`**            | `string`           | `not null`
# **`password_digest`**  | `string`           |
# **`tokens`**           | `text`             |
# **`created_at`**       | `datetime`         | `not null`
# **`updated_at`**       | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_users_on_email` (_unique_):
#     * **`email`**
# * `index_users_on_username` (_unique_):
#     * **`username`**
#


class Account < User
    
    after_create :set_default_role
    def set_default_role
        self.add_role :guest
        self.save
    end

end
