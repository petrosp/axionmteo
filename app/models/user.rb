# frozen_string_literal: true

# ## Schema Information
#
# Table name: `users`
#
# ### Columns
#
# Name                   | Type               | Attributes
# ---------------------- | ------------------ | ---------------------------
# **`id`**               | `uuid`             | `not null, primary key`
# **`username`**         | `string`           | `not null`
# **`email`**            | `string`           | `not null`
# **`password_digest`**  | `string`           |
# **`tokens`**           | `text`             |
# **`created_at`**       | `datetime`         | `not null`
# **`updated_at`**       | `datetime`         | `not null`
#
# ### Indexes
#
# * `index_users_on_email` (_unique_):
#     * **`email`**
# * `index_users_on_username` (_unique_):
#     * **`username`**
#


class User < ActiveRecord::Base      
  include HasSecureTokens
  include HasFulltextSearch

  has_secure_password
  has_secure_tokens
  rolify      

  validates_uniqueness_of :email,
                          :username  
                          
  # Find a user in the cache with matching 
  # id and token
  def self.find_in_cache(id, token)
    Rails.cache.fetch(["User", id, token], expires_in: 5.minutes) do 
      where('id = ? and tokens like ?', id, "%#{token}%").first 
    end
  end                           
 
end
